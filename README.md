TinyTitan@GT Build Guide

Equipment:

1.	2-9 raspberry Pis 

2.	2-9 Ethernet cables

3.	HDMI to DVI/VGA cable

4.	16 port Ethernet switch 

5.	2-9 micro usb cable

6.	2-9 8GB micro sd card

7.	USB hubs 

8.	Raspberry pi cases  

9.	X box controller

Install OS: 
First download the raspbian package from the raspberry pi website and put it on the sd card. Then insert sd card to the raspberry pi and power it on. Follow the instruction and complete the OS installation. 

Step One:
After the OS has been properly installed, download the pi_setup.sh script from the github. The script will update the system, install the packages needed, and change the network settings. This step will require the raspberry pi to have internet access. 

To download the set up script: 

$ curl -kfsSLO https://raw.github.com/TinyTitan/TinySetup/master/pi_setup.sh

(This link will be replaced by the bitbucket link later)

Backup the network interface:

$ cd /etc/network 

$ cp interfaces interfaces.bak

Then run the pi_setup.sh script. Type the node number when prompted, starts from one. 

Repeat the above steps for each raspberry pis. And connect all of them together using the Ethernet switch when finished. 

Step Two:

With all raspberry pis connected. on the pi that was given a node number of 1 login and download/run the pi_post_setup.sh script. The script will assume all the pis use the default username and password.

$ git clone https://github.com/TinyTitan/TinySetup.git

$ cd TinySetup

$ bash pi_post_setup.sh (later replaced by the bitbucket link)










Applications: 

TinySPH
TinySPH is a parallel 2D Smoothed Particle Hydrodynamics(SPH) code, designed to run in real time on the Oak Ridge Leadership Computing Facility’s “Tiny Titan” Raspberry Pi cluster. This application was designed to demonstrate distributed computing concepts and while it works best in a distributed environment it is possible to run on a shared memory system such as a single multicore processor. Although the code is designed for Tiny Titan it should, perhaps with some small build tweaks, compile and run on various flavors of Linux and OS X. The code makes use of MPI for distributed computing and requires at least two mpi processes(1 render, 1 compute).
 
For full details please see the TinySPH github page